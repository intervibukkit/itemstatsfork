package ru.dondays.itemstats;

import com.google.common.collect.Lists;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.List;

public class ItemListener
    implements Listener {

    private List<Material> logKills = Lists.newArrayList();
    private List<Material> logBreak = Lists.newArrayList();

    private String craftFormat;
    private String mobKillsFormat;
    private String playerKillsFormat;
    private String breakFormat;
    private String bowFormat;

    private String s2;
    private String s3;
    private String s4;
    private String s5;

    void load() {
        reset();
        ItemStats.getInstance().reloadConfig();
        for (String s : ItemStats.getInstance().getConfig().getStringList("logKills")) logKills.add(Material.valueOf(s));
        for (String s : ItemStats.getInstance().getConfig().getStringList("logBreak")) logBreak.add(Material.valueOf(s));
        craftFormat = ItemStats.getInstance().getConfig().getString("Strings.craft.format").replace('&', '§');
        mobKillsFormat = ItemStats.getInstance().getConfig().getString("Strings.mobKills.format").replace('&', '§');
        s2 = ItemStats.getInstance().getConfig().getString("Strings.mobKills.split").replace('&', '§');
        playerKillsFormat = ItemStats.getInstance().getConfig().getString("Strings.playerKills.format").replace('&', '§');
        s3 = ItemStats.getInstance().getConfig().getString("Strings.playerKills.split").replace('&', '§');
        breakFormat = ItemStats.getInstance().getConfig().getString("Strings.break.format").replace('&', '§');
        s4 = ItemStats.getInstance().getConfig().getString("Strings.break.split").replace('&', '§');
        bowFormat = ItemStats.getInstance().getConfig().getString("Strings.bow.format").replace('&', '§');
        s5 = ItemStats.getInstance().getConfig().getString("Strings.bow.split").replace('&', '§');
    }

    void reset() {
        logKills.clear();
        logBreak.clear();
        craftFormat = null;
        mobKillsFormat = null;
        s2 = null;
        playerKillsFormat = null;
        s3 = null;
        breakFormat = null;
        s4 = null;
        bowFormat = null;
        s5 = null;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCraft(CraftItemEvent e) {
        if(!ItemStats.getInstance().getConfig().getBoolean("Strings.craft.enable")) return;
        if(e.getWhoClicked().getGameMode() == GameMode.CREATIVE && !ItemStats.getInstance().getConfig().getBoolean("enableStatisticInCreative"))
            return;
        if (!e.getResult().equals(Result.ALLOW)) return;
        ItemStack item = e.getCurrentItem();
        if (ItemStats.getInstance().getConfig().getBoolean("Strings.craft.inLists")) {
        	Material type = item.getType();
        	if (!logKills.contains(type) && !logBreak.contains(type)) return;
        }
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Lists.newArrayList(craftFormat.replace("%name%", e.getWhoClicked().getName())));
        item.setItemMeta(meta);
    }
    
    private ItemMeta getKillsMeta(ItemStack item, boolean player) {
    	if (logKills.contains(item.getType())) {
    		ItemMeta meta = item.getItemMeta();
        	List<String> lore = meta.getLore();
            List<String> newLore = Lists.newArrayList();
            if(lore != null) {
            	String search = player ? playerKillsFormat.replace("%num%", "") : mobKillsFormat.replace("%num%", "");
                for(String l : lore) {
                    if(!l.contains(search)) {
                        newLore.add(l);
                        continue;
                    }
                    String breaked = String.valueOf(player ? Integer.valueOf(l.split(s3)[1])+1 : Integer.valueOf(l.split(s2)[1])+1);
                    newLore.add(player ? playerKillsFormat.replace("%num%", breaked) : mobKillsFormat.replace("%num%", breaked));
                }
            }
            meta.setLore(newLore);
            return meta;
        }
    	return null;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDeath(EntityDeathEvent e) {
        if(e.getEntity().getKiller() == null || e.getEntity().getKiller().getType() != EntityType.PLAYER || e.getEntity().getType() == EntityType.PLAYER)
            return;
        if(e.getEntity().getKiller().getGameMode() == GameMode.CREATIVE && !ItemStats.getInstance().getConfig().getBoolean("enableStatisticInCreative"))
            return;
        Player killer = e.getEntity().getKiller();
        ItemMeta meta = getKillsMeta(killer.getItemInHand(), false);
        if (meta != null) killer.getItemInHand().setItemMeta(meta);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent e) {
        if(e.getEntity().getKiller() == null || e.getEntity().getKiller().getType() != EntityType.PLAYER || e.getEntity().getType() != EntityType.PLAYER)
            return;
        if(e.getEntity().getKiller().getGameMode() == GameMode.CREATIVE && !ItemStats.getInstance().getConfig().getBoolean("enableStatisticInCreative"))
            return;
        Player killer = e.getEntity().getKiller();
        ItemMeta meta = getKillsMeta(killer.getItemInHand(), false);
        if (meta != null) killer.getItemInHand().setItemMeta(meta);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBreak(BlockBreakEvent e) {
        if(e.getPlayer().getGameMode() == GameMode.CREATIVE && !ItemStats.getInstance().getConfig().getBoolean("enableStatisticInCreative"))
            return;
        ItemStack item = e.getPlayer().getItemInHand();
        if (logBreak.contains(item.getType())) {
        	ItemMeta meta = item.getItemMeta();
        	List<String> lore = meta.getLore();
            List<String> newLore = Lists.newArrayList();
            if(lore != null) {
            	String search = breakFormat.replace("%num%", "");
                for(String l : lore) {
                    if(!l.contains(search)) {
                        newLore.add(l);
                        continue;
                    }
                    newLore.add(breakFormat.replace("%num%", String.valueOf(Integer.valueOf(l.split(s4)[1])+1)));
                }
            }
            meta.setLore(newLore);
            item.setItemMeta(meta);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onHit(ProjectileLaunchEvent e) {
        if(!(e.getEntity().getShooter() instanceof Player)) return;
        if(((Player)e.getEntity().getShooter()).getGameMode() == GameMode.CREATIVE && !ItemStats.getInstance().getConfig().getBoolean("enableStatisticInCreative"))
            return;
        Player shooter = (Player)e.getEntity().getShooter();
        if(shooter.getItemInHand() == null || shooter.getItemInHand().getType() != Material.BOW) return;
        ItemStack item = shooter.getItemInHand();
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        List<String> newLore = Lists.newArrayList();
        if(lore != null) {
            for(String l : lore) {
                if(!l.contains(bowFormat.replace("%num%", ""))) {
                    newLore.add(l);
                    continue;
                }
                newLore.add(bowFormat.replace("%num%", String.valueOf(Integer.valueOf(l.split(s5)[1])+1)));
            }
        }
        meta.setLore(newLore);
        item.setItemMeta(meta);
    }
}

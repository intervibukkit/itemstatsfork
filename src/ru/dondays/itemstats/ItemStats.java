package ru.dondays.itemstats;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemStats
    extends JavaPlugin {

    private static ItemStats instance;
    private ItemListener listener;

    @Override
    public void onEnable() {
        instance = this;
        this.saveDefaultConfig();
        this.listener = new ItemListener();
        this.listener.load();
        this.getServer().getPluginManager().registerEvents(listener, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("itemstats.reload")) {
            sender.sendMessage("§cУ вас недостаточно прав!");
            return false;
        }
        if(args.length == 1 || !args[0].equalsIgnoreCase("reload")) {
            sender.sendMessage("§eИспользуйте - §c/itemstats reload");
            return false;
        }
        HandlerList.unregisterAll(listener);
        this.getServer().getPluginManager().registerEvents(listener, this);
        this.listener.load();
        return true;
    }

    static ItemStats getInstance() {
        return instance;
    }
}
